﻿using System;

namespace comp5002_10010257_assessment01
{
    class Program
    {
        static void Main(string[] args)
        {
           
           Console.Clear();
           //Start the program with Clear();
            
            var decision = "";                                                                         
            var userName = "";              
            double num1 = 0;    
            //Declared variables needed, first user input number, users name and their decision answer if they want to add another number 

            Console.WriteLine("Hello and welcome to my store!"); 
            //Print welcome text to screen

            Console.WriteLine("What is your name?");                                                   
            userName = Console.ReadLine(); 
            //Ask users name and store as userName variable 

            Console.WriteLine($"Hello {userName}!");                        
            //Print userName variable along with a greating 

            Console.WriteLine("Please enter your two decimal place number.");                           
            num1 = Double.Parse(Console.ReadLine()); 
            //Ask users initial number and store as a variable

            Console.WriteLine("Would you like to enter a second two decimal number? yes or no?");             
            decision = Console.ReadLine(); 
            //Ask for a decision on if the users wants to add a second number and store the decision as a variable 

            if (decision=="yes") 
            //If decision is yes, execute code block under if.                                                                                  
            {
                Console.WriteLine("What is the second two decimal number you wish to add?");  
                num1 += Double.Parse(Console.ReadLine());
                Console.WriteLine($"The total is {num1}");
                Console.WriteLine("Press any key to calculate gst"); 
                Console.ReadKey();
                //Ask what the second number they wish to add is, add the second number to the num1 variable, then print the num1 variable                                                      
            }
            else
            //If decision is anything other than yes the code executed is the block under else
            {
                Console.WriteLine($"Your total before gst is {num1}.");
                 //Print num1 variable. Because the decision wasnt yes, the num1 variable stays as the initial number
                                                                                                    
            }
            Console.WriteLine($"and your total after gst is {(num1*1.15)}");
             //Print num1 variable, either the initial number on its own or the two user inputed numbers, after being multiplied by the gst rate

            Console.WriteLine("Thank you for shopping with us, please come again!");  
             //Print Goodbye text                 
           
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program...");
            Console.ReadKey();
            //Reset color to default and ask for user input in order to close the program
    }
    }
}
